import { Component, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { MissionService } from '../Servicios/mission.service';
import {ImagenBoton} from '../Modules/ImagenBoton';
// import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  diccionarioPrincipal =  Array<ImagenBoton>();
  public diccionarioImagenes = Array<ImagenBoton>();

  idiomaActual:string;
  indiceIdioma:number = 1;


  constructor(private router: Router,
     private missionService: MissionService) {


    this.diccionarioPrincipal = this.missionService.Diccionario();

    missionService.missionConfirmed$.subscribe(
      astronaut => {
        console.log("En home:"+astronaut);
        this.indiceIdioma = astronaut;
        if(astronaut == 0)
        {

          this.idiomaActual = "Ruso";
        }
        else{
          this.idiomaActual = "Espaniol";
        }


        this.diccionarioPrincipal = this.missionService.Diccionario();

      });

      missionService.miMision$.subscribe(
        astronaut => {
          console.log("Segunda mision:"+astronaut);
          this.diccionarioPrincipal = this.missionService.Diccionario();
  
        });
  }

  ngOnInit() {    
    
  }

  Accion(indice) {   
    console.log(indice);    
this.missionService.Reproducir(indice);

// this.cassettera.Reproducir(evento.toElement.id);
  }

  salir()
  {
    this.router.navigate(['ingreso']);
  }
}
