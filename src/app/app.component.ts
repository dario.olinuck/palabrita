import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


// agregados
import { Router } from '@angular/router';
import {timer} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {


   // rootPage:any= 'LoginPage';
   showSplash: boolean = true;
   caminoInicial: string = '../../assets/audio/lalala.wav';
   acdc: any;
 

  public appPages = [
    {
      title: '',
      url: '/idiomas',
      icon: 'home'
    },
    {
      title: '',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private router:Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {

    this.initializeApp();
  }

  initializeApp() {

    this.platform.ready().then(() => {

      timer(3800).subscribe(() => this.showSplash = false);

    
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.iniciarlizarAudios();
      this.router.navigateByUrl('ingreso');
    });
  }

  
  iniciarlizarAudios() {
    this.acdc = new Audio();
    this.acdc.src = this.caminoInicial;
    this.acdc.load();
    this.acdc.play();       
  }
}
