import { Injectable } from '@angular/core';
import { miIcono } from '../Modules/miIcono';


@Injectable({
  providedIn: 'root'
})
export class IconosService {


  ruso = new miIcono(0,"Ruso","../../assets/russia.svg");
  espaniol = new miIcono(1,"Español","../../assets/argentina.svg");
 
  colores = new miIcono(0,"Colores","../../assets/colors.svg");
  numeros = new miIcono(1,"Numeros","../../assets/letter.svg");
  animales = new miIcono(2,"Animales","../../assets/cow.svg");
  

  ///Colores numeros animales


  constructor() { }

  public Idiomas() {

    let lista = new Array<miIcono>();
    lista.push(this.ruso);
    lista.push(this.espaniol);

    return lista;
  }

  public Opciones(){
    let lista = new Array<miIcono>();
    lista.push(this.colores);
    lista.push(this.numeros);
    lista.push(this.animales);

    return lista;
  }
}
