import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-logueo',
  templateUrl: './error-logueo.page.html',
  styleUrls: ['./error-logueo.page.scss'],
})
export class ErrorLogueoPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  volverALogueo(){
    this.router.navigate(['ingreso']);
  }


  registrarse(){
    this.router.navigate(['registro']);
  }

}
